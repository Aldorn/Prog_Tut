/**
 * Main class containing the main method.
 */
public class Main {
    /**
     * The main method called by the JRE
     */
    public static void main(String[] args) {
        Point v = new Point(42, 4711);
        System.out.println(v.toString());
        System.out.println(new Main());
    }
}
