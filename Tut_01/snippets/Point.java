/**
 * Simple two dimensional Point.
 * Note that no encapsulation has been used, which is bad style.
 */
public class Point {
    /**
     * The x coordinate.
     */
    public int x;
    /**
     * The y coordinate.
     */
    public int y;
    
    /**
     * Creates a new Point with the specified coordinates.
     */
    public Point(int x, int y) {
        this.x = x;
        this.y = y;
    }
    
    /**
     * Returns a String representation of the point.
     */
    public String toString() {
        return "Point[" + x + "," + y + "]";
    }
}
