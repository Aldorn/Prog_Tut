public class Vector {
  private int x, y;
  public void move(int x) {
    for (int i = 0; i < x; ++i) {
      int y = 2*x;
      x = y;
      System.out.println("" + x + ", " + y);
    }
    int i = 0;
    while (y < i*x) {
      System.out.println("b " + x + ", " + y);
      y += x;
      ++i;
    }
    this.x = x;
  }

  public Vector(int x, int y) {
    this.x = x; this.y = y;
  }

  public String toString() {
    return "(" + x + ", " + y + ")";  
  }

  public static void main(String[] args) {
    Vector v = new Vector(1880, 42);
    System.out.println(v);
    v.move(127);
    System.out.println(v);
  }
}
