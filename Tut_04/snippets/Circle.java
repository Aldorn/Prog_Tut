public class Circle {
    private double radius;
    /**
     * Creates a new Circle with the specified radius.
     */
    Circle (double radius) {
        this.radius = radius;
    }
    /**
     * Returns the radius.
     */
    double getRadius() {
        return radius;
    }
    /**
     * Calculates the circumference.
     */
    double getCircumference() {
        return radius * 2 * 3.14;//Using 3.14 as approximation for pi
    }
}
