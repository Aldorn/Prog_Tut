public class Vergleich {
  public static void main(String args[]) {
    int a = Integer.parseInt(args[0]); //Interpretiert das erste Argument als Zahl
    int b = Integer.parseInt(args[1]);
    System.out.println(a == b);
  }
}
