public class StringIteration {
    public static void main(String args[]) {
        int min = Math.min(args[0].length(), args[1].length());
        int max = Math.max(args[0].length(), args[1].length());
        System.out.println(args[0]);
        System.out.println(args[1]);
        for (int i = 0; i < min; ++i) {
            if (args[0].charAt(i) == args[1].charAt(i)) {
                System.out.print(" ");
            } else {
                System.out.print("X");
            }
        }
        for (int i = min; i < max; ++i) {
            System.out.print("|");
        }
        System.out.println();
    }
}
