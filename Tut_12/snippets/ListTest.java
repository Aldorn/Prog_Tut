package edu.kit.informatik;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import org.junit.Test;

public class ListTest {
	@Test
	public void testAddSorted() {
		SortedAppendList<String> l = new LinkedSortedAppendList<>();
		l.addSorted("b");
		l.addSorted("a");
		SortedIterator<String> it = l.iterator();
		assertThat(it.hasNext(), is(true));
		assertThat(it.next(), is("a"));
		assertThat(it.hasNext(), is(true));
		assertThat(it.next(), is("b"));
		assertThat(it.hasNext(), is(false));
	}
}
