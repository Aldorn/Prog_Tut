/**
 * {@link Caesar} contains the needed methods to encrypt a given text and a key using Caesar Encryption.
 * 
 * @author Annika Berger
 *
 */
public class Caesar {
	
	/**
	 * Does an Caesar Encryption on the given text using the key.
	 * 
	 * @param args The text to encrypt as first argument and the key as second argument
	 */
	public static void main(String[] args) {
		String text = args[0];
		char key = args[1].charAt(0);
		String secret = "";
		
		// calculate number to be added to letters
		int diff = key - 'a';
		
		char currentLetter;
		
		// iterating through the text to encrypt each letter
		for (int i = 0; i < text.length(); i++) {
			currentLetter = text.charAt(i);
			
			// small letters
			if ('a' <= currentLetter && currentLetter <= 'z') {
				secret += encryptRange(currentLetter, diff, 'a', 'z');
				
			// capital letters
			} else if ('A' <= currentLetter && currentLetter <= 'Z') {
				secret += encryptRange(currentLetter, diff, 'A', 'Z');
				
			// special characters
			} else {
				secret += currentLetter;
			}
		}
		
		// Print the encrypted text
		System.out.println(secret);

	}
	
	/**
	 * Encrypts a letter in a given range with a given key.
	 * 
	 * @param letter the letter to encrypt
	 * @param diff the key for encryption (number to be added to letter)
	 * @param startOfRange first letter of the range
	 * @param endOfRange last letter of the range
	 * @return the encrypted letter as char
	 */
	private static char encryptRange(char letter, int diff, char startOfRange, char endOfRange) {
		
		// shift letter
		int distanceFromStart = letter - startOfRange;
		// encrypt letter
		int encryptedDistanceFromStart = (distanceFromStart + diff) % (endOfRange - startOfRange + 1);
		// shift back and cast to char
		char encryptedLetter = (char) (encryptedDistanceFromStart + startOfRange);
		
		
		return encryptedLetter;
	}

}
