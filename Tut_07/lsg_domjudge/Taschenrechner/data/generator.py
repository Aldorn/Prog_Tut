#!/usr/bin/python3

import math
from random import randint, choice
from argparse import ArgumentParser as Parser
from pathlib import Path

def r():
    return randint(-1000, 10000)

ops = ['+', '-', '*', '/']
pyops = ['+', '-', '*', '//']

def create_test(i=0, n=4):
    x = r()
    y = r()
    instring = '{0} {2} {1}'.format(x, y, ops[i % len(ops)])
    outstring = str(eval('{0} {2} {1}'.format(x, y, pyops[i % len(pyops)])))
    return (instring, outstring)

def main():
    parser = Parser(description='Generate test cases')
    parser.add_argument('-n', '--number', default=10, type=int, dest='n',\
        help = 'number of test cases to generate')
    parser.add_argument('-f', '--filename', default='test', type=str, dest='f',\
        help = 'base filename used')
    parser.add_argument('-p', '--path', default='secret', type=str, dest='p',\
        help = 'path to store generated test files (default secret)')

    args = parser.parse_args()
    
    cnt = args.n
    name = args.f
    testpath = args.p
    p = Path(testpath)
    if not p.exists():
        p.mkdir(parents=True)
    print(args)
    print(cnt, name, testpath)
    
    for i in range(cnt):
        (instring, outstring) = create_test(i, cnt)
        filename = '{}/{:}_{:0>{width}}'.format(testpath, name, i, width=len(str(cnt - 1)))
        with open(filename + '.in', 'w') as infile:
            print(instring)
            infile.writelines(instring)
        with open(filename + '.ans', 'w') as outfile:
            print(outstring)
            outfile.write(outstring)

        #print(filename, p, h, d)
    pass

main()
