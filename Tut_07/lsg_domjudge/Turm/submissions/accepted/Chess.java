class Chess {
    public static final int CHESS_SIZE = 8;
    private static final String SYMBOLS[] = {"_", "#"};
    
    
    public static void printChessLine(int line, int t_x, int t_y) {
        for (int i = 0; i < CHESS_SIZE; ++i) {
            if (line == t_y) {
                if (i == t_x) {
                    System.out.print("T");
                } else {
                    System.out.print("X");
                }
            } else {
                if (i == t_x) {
                    System.out.print("X");
                } else {
                    System.out.print(SYMBOLS[(i + line) % 2]);
                }
            }
            System.out.print(" ");
        }
        System.out.println();
    }
    
	public static void main(String[] args) {
        int tx = args[0].charAt(0) - 'a';
        int ty = 8 - args[0].charAt(1) + '0';
        for (int i = 0; i < CHESS_SIZE; ++i) {
            printChessLine(i, tx, ty);
        }
	}
}
		
