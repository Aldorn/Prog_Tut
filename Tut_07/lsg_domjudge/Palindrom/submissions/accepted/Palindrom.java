/**
 * Checks if a given word is a palindrom.
 * 
 * @author moritz
 *
 */
public class Palindrom {
	public static void main(String[] args) {
		int length = args[0].length();
		boolean isPalindrom = true;
		// Iterate over half of the word's length. Compare the first
		// with the last character, the second one with the second one to the last and
		// so on.
		for (int i = 0; i < length / 2; i++) {
			if (args[0].charAt(i) != args[0].charAt(length - 1 - i)) {
				isPalindrom = false;
			}
		}
		System.out.println(isPalindrom);
	}
}
