public class Fibo {

    public static long fibo(int n) {
        //Bei dieser Aufgabe muss long verwendet werden, da fibo(92) nicht mehr in einen int passt
        long x1 = 1;
        long x2 = 1;
        for (int i = 1; i < n; ++i) {
            long t = x1;
            x1 = x2;
            x2 = x1 + t;
        }   
        return x1;
    }

	public static void main(String[] args) {
        int n = Integer.parseInt(args[0]);
        System.out.println(fibo(n));
	}
}
