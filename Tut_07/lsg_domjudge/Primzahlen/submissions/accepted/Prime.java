public class Prime {
    public boolean isPrime(int i) {
        if (i < 2) return false;
        if (i != 2 && i % 2 == 0) return false;
        //Suche bis zur Wurzel der Zahl
        for (int t = 3; t <= Math.sqrt(i) + 1; t += 2) {
            if (i % t == 0) return false;
        }
        return true;
    }

	public static void main(String[] args) {
        int p = Integer.parseInt(args[0]);
        Prime prime = new Prime();
        System.out.println(prime.isPrime(p));
	}
}
