# First parameter: countdown start + 1
# second parameter: outfile

top=$(($1-1))
outfile="$2"

for i in $(seq $top -1 0); do
	echo $i >> $outfile
done

