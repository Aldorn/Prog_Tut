public class Charcount {
	public static void main(String[] args) {
        String w = args[0];
        int a = 0, e = 0;
        for (int i = 0; i < w.length(); ++i) {
            char c = w.charAt(i);
            switch (c) {
            case 'a':
            case 'A':
                ++a;
                break;
            case 'e':
            case 'E':
                ++e;
                break;
            default:
                break;
            }
        }
        System.out.println(e > a);
	}
}
		
