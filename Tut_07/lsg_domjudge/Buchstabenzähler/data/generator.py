#!/usr/bin/python3

import math
from random import randint as r, choice
from argparse import ArgumentParser as Parser
from pathlib import Path
import re


alphabet = "".join((chr(c) for c in range(ord('a'), ord('z'))))
pat = re.compile(r'^\w{3,}$')

with open('wordlist.txt') as f:
    words = [w.strip() for w in f.readlines() if pat.match(w.strip())]

bool_lit = {True:'true', False:'false'}

def count_chr(char, word):
    cnt = 0
    for c in word:
        if c == char: cnt += 1
    return cnt

def a_e(w):
    a = count_chr('a', w.lower())
    e = count_chr('e', w.lower())
    return e > a

def create_test(i=0, n=4):
    w = choice(words)
    b = bool_lit[a_e(w)]
    return (w, b)

def main():
    parser = Parser(description='Generate test cases')
    parser.add_argument('-n', '--number', default=10, type=int, dest='n',\
        help = 'number of test cases to generate')
    parser.add_argument('-f', '--filename', default='test', type=str, dest='f',\
        help = 'base filename used')
    parser.add_argument('-p', '--path', default='secret', type=str, dest='p',\
        help = 'path to store generated test files (default secret)')

    args = parser.parse_args()
    
    cnt = args.n
    name = args.f
    testpath = args.p
    p = Path(testpath)
    if not p.exists():
        p.mkdir(parents=True)
    print(args)
    print(cnt, name, testpath)
    
    for i in range(cnt):
        (instring, outstring) = create_test(i, cnt)
        filename = '{}/{:}_{:0>{width}}'.format(testpath, name, i, width=len(str(cnt - 1)))
        with open(filename + '.in', 'w') as infile:
            print(instring)
            infile.writelines(instring)
        with open(filename + '.ans', 'w') as outfile:
            print(outstring)
            outfile.write(outstring)

        #print(filename, p, h, d)
    pass

main()
