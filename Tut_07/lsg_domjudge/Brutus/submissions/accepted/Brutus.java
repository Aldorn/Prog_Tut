public class Brutus {

	/**
	 * The term used to identify a correctly decrypted message.
	 */
	private final static String SEARCHED_TERM = "public static void main";

	public static void main(String[] args) {
		String chiffre = args[0];
		boolean foundKey = false;
		String decrypted = "";
		int keyDiff;
		for (keyDiff = 0; keyDiff <= 'z' - 'a'; keyDiff++) {
			decrypted = decrypt(keyDiff, chiffre);
			if (decrypted.contains(SEARCHED_TERM)) {
				foundKey = true;
				break;
			}
		}

		if (!foundKey) {
			System.out.println("Verrat!");
		} else {
			char key = encryptRange('a', keyDiff, 'a', 'z');
			System.out.println(key);
			System.out.println(decrypted);
		}
	}

	/**
	 * Decrypts the given {@code chiffre} using {@code diff} as Caesar shift.
	 *
	 * @param diff The difference to shift characters by.
	 * @param chiffre The text to decrypt.
	 * @return The decrypted text.
	 */
	private static String decrypt(int diff, String chiffre) {
		String decrypted = "";
		for (int i = 0; i < chiffre.length(); i++) {
			char chiffreChar = chiffre.charAt(i);
			if ('a' <= chiffreChar && chiffreChar <= 'z') {
				decrypted += decryptRange(chiffreChar, diff, 'a', 'z');
			} else if ('A' <= chiffreChar && chiffreChar <= 'Z') {
				decrypted += decryptRange(chiffreChar, diff, 'A', 'Z');
			} else {
				decrypted += chiffreChar;
			}
		}
		return decrypted;
	}

	/**
	 * Decrypts a letter in a given range with a given key. Inverse method of {@link #encryptRange}.
	 *
	 * @param letter the letter to decrypt
	 * @param diff the key for decryption (number to be added to letter)
	 * @param startOfRange first letter of the range
	 * @param endOfRange last letter of the range
	 * @return the decrypted letter as char
	 */
	private static char decryptRange(char letter, int diff, char startOfRange, char endOfRange) {
		// This method's implementation has several flaws. However, it’s easy to understand and does
		// not use any library methods.
		// Note: #encryptRange uses the remainder operator (%), that does *not* calculate the modulus
		// for negative numbers. We thus add the rang difference to ensure we have positive number
		return encryptRange(letter, -diff + (endOfRange - startOfRange + 1), startOfRange, endOfRange);
	}

	/**
	 * Encrypts a letter in a given range with a given key.
	 * This method is reused from the solution for Caesar.
	 * 
	 * @param letter the letter to encrypt
	 * @param diff the key for encryption (number to be added to letter)
	 * @param startOfRange first letter of the range
	 * @param endOfRange last letter of the range
	 * @return the encrypted letter as char
	*/
	private static char encryptRange(char letter, int diff, char startOfRange, char endOfRange) {
		// shift letter
		int distanceFromStart = letter - startOfRange;
		// encrypt letter
		int encryptedDistanceFromStart = (distanceFromStart + diff) % (endOfRange - startOfRange + 1);
		// shift back and cast to char
		char encryptedLetter = (char) (encryptedDistanceFromStart + startOfRange);
		return encryptedLetter;
	}
}
