public class Time {
	private int hour, minute, second;

	public Time add(Time other) {
		int h, m, s; //new values
		int oM, oS; //carry
		s = (second + other.second) % 60;
		oS = (second + other.second) / 60;
		m = (minute + other.minute + oS) % 60;
		oM = (minute + other.minute) / 60;
		h = (hour + other.hour + oM) % 60;
		return new Time(h, m, s);
	}
}
