public class Time {
	private final int hour, minute, second;
	
	public isBefore(Time other) {
		if (other.hour > hour) return true;
		else if (other.minute > minute) return true;
		else if (other.second > second) return true;
		else return false;
	}
}
