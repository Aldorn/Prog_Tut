public class Main {
    public static void main(String[] args) {
        Vector v = new Vector(1, 1);
        System.out.println(v.equals(v));
        System.out.println(v.equals(null));
        System.out.println(v.equals("null"));
        Vector b = new Vector(10, 42);
        System.out.println(v.equals(b));
        System.out.println(b.equals(v));
        Vector c = new Vector(1, 1);
        System.out.println(v.equals(c));
        Object o = c;
        System.out.println(v.equals(o));
        System.out.println(o.equals(v));
    }
}
