public class Time {
	private int hour, minute, second;

	public Time minus(Time other) {
		int h, m, s;
		s = second - other.second;
		m = minute - other.minute;
		h = hour - other.hour;
		if (s < 0) { s += 60; m -= 1;}
		if (m < 0) { m += 60; h -= 1;}
		h %= 60;
		return new Time(h, m, s);
	}
}
