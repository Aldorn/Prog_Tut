public final class Vector {
    private int x, y;
    
    public Vector(int x, int y) {
        this.x = x;
        this.y = y;
    }   

    int getX() {
        return x;
    }
    
    int getY() {
        return y;
    }
    
    Vector add(Vector v) {
        return new Vector(x + v.x, y + v.y);
    }
    
    Vector subtract(Vector v) {
        return new Vector(x - v.x, y - v.y);
    }
    
    @Override
    public boolean equals(Object o) {
        if (o == null) return false;
        else if (this == o) return true;
        else if (o instanceof Vector) {
            Vector n = (Vector) o;
            return  (getX() == n.getX() && getY() == n.getY());
        }
        return super.equals(o);
    }
}
