Hinweis
=======
Die in diesem Ordner angegebenen Lösungen sind alle als Beispiel zu verstehen. Ist gibt oft auch alternative Wege, die gewünschte Ausgabe zu erreichen. 

Ausführen
=========
Um die einzelnen Programme auszuführen, müssen die erst kompiliert werden. Erst dann kann die Java-VM den Bytecode ausführen.
Zuerst müssen die Quelldateien in einen beliebigen Ordner entpackt werden.


Linux/MacOS
-----------
* Terminal öffnen (dem Anwender überlassen, zu uneinheitlich, um hier genauer darauf einzugehen)
* mit *cd* in den Ordner wechseln, in den die Dateien entpackt wurden

Windows
-------
* Kommandozeile öffnen:
    * Startmenü -> *cmd* suchen -> starten
    * mit *cd* in den Ordner wechseln, in den die Dateien entpackt wurden
    * *dir* zeigt alle Dateien im aktuellen Arbeitsverzeichnis an, damit kann man prüfen, ob man richtig ist
    * Alternativ: *Shift+Rechtsklick* im Explorer, im Ordner, in dem die Dateien liegen -> *Kommandozeile hier öffnen*
    
Alle
----
Wenn man das Terminal geöffnet hat und im richtigen Arbeitsverzeichnis angekommen ist kann man das Programm dann kompilieren und ausführen:
* um die Datei X.java zu kompilieren, *javac X.java* im Terminal eingeben
* ausführen mit *java X*
