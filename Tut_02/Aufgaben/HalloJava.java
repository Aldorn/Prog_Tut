/**
 * Klasse, die nur die main-Methode enthält.
 */
public class HalloJava {
    /**
     * main-Methode. Gibt beim Aufruf "Hallo Welt!" und in einer zweiten Zeile 
     * "Hallo Java!" auf der Standardausgabe (meist Terminal) aus.
     * @param args Array der Kommandozeilenargumente
     */
    public static void main(String args[]) {
        System.out.println("Hallo Welt!");
        //println fürgt automatisch einen Zeilenumbruch ein. 
        //Wir brauchen deshalb nicht explizit einen anzugeben
        System.out.println("Hallo Java!");
    }
}
