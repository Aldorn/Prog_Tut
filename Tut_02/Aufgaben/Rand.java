/**
 * Klasse, die nur die main-Methode enthält.
 * Die Klasse heißt hier Rand und nicht Random, damit es 
 * auf Seiten des Programmierers nicht zu Verwechslungen kommt.
 * Java könnte die Klasse ohne Probleme von java.util.Random unterscheiden, 
 * da es sich um ein anderes Package handelt.
 */
public class Rand {
    /**
     * main-Methode. Gibt beim Aufruf "Hallo Welt!" auf der Standardausgabe (meist Terminal) aus.
     * @param args Array der Kommandozeilenargumente
     */
    public static void main(String args[]) {
        System.out.println("Zufallszahl: " + Math.random());
    }
}
