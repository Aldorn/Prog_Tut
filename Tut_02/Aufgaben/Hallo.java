/**
 * Klasse, die nur die main-Methode enthält.
 */
public class Hallo {
    /**
     * main-Methode. Gibt beim Aufruf "Hallo Welt!" auf der Standardausgabe (meist Terminal) aus.
     * @param args Array der Kommandozeilenargumente
     */
    public static void main(String args[]) {
        System.out.println("Hallo Welt!");
    }
}
