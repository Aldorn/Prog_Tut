/**
 * Klasse, die nur die main-Methode enthält.
 */
public class Floating {
    /**
     * main-Methode. 
     * Führt die gleiche Rechnung einmal mit float und einmal mit double Werten durch. 
     * Man sieht einen Unterschied in der Genauigkeit.
     * Das rechnerisch erwartete Ergebnis ist übrigens 10.
     * @param args Array der Kommandozeilenargumente
     */
    public static void main(String args[]) {
        System.out.println(evalFloat());
        System.out.println(evalDouble());
    }

    /**
     * Führt eine kleine Berechnung auf float Werten durch. 
     * Dabei sollten Probleme in der Gleikommagenauigkeit zutage treten.
     */
    public static float evalFloat() {
        float a = 200.1f;
        float b = 600.1f;
        float c = 300.1f;
        float d = 400.1f;
        return a * b - c * d;
    }

    /**
     * Führt eine kleine Berechnung auf double Werten durch. 
     * Dabei sollten Probleme in der Gleikommagenauigkeit zutage treten.
     */
    public static double evalDouble() {
        double a = 200.1;
        double b = 600.1;
        double c = 300.1;
        double d = 400.1;
        return a * b - c * d;
    }

}
