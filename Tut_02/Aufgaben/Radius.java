/**
 * Klasse, die nur die main-Methode enthält.
 */
public class Radius {
    /**
     * main-Methode. Gibt den Umfang eines Kreises mit Radius 7.2 aus.
     * @param args Array der Kommandozeilenargumente
     */
    public static void main(String args[]) {
        double r = 7.2;
        double pi = 3.14; //Alternativ geht auch pi = Math.PI
        double circumference = 2 * pi * r;
        System.out.println(circumference);
    }
}
