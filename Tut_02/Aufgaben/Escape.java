/**
 * Klasse, die nur die main-Methode enthält.
 */
public class Escape {
    /**
     * main-Methode. Gibt einen String mit vielen Zeichen aus, die Sonderbehandlung verlangen.
     * @param args Array der Kommandozeilenargumente
     */
    public static void main(String args[]) {
        //Wichtig ist, dass die " innerhalb des Strings auf ein \ folgen, 
        //damit sie nicht die Zeichenkette beenden.
        //Derartige Konstruktionen mit einem \, auf das Zeichen folgen nennt man Escape-Sequenz.
        //Es zeigt sich hier ein Problem in der Windowsspezifischen Verwendung 
        //von \ als Trennzeichen für Pfade: die Darstellung in Porgrammen ist kompliziert.
        System.out.println("Windowspfad: \"C:\\Windows\\System32\\\"");
    }
}
