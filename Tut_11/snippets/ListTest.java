package edu.kit.informatik;

import edu.kit.informatik.konto.Account;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import org.junit.Test;

public class ListTest {
  @Test
  public void testAdd() {
    MinimalList l = new MinimalList();
    Account a1 = new Account(1, 10), a2 = new Account(2, 11);

    assertThat(l.get(0), is(nullValue()));
    l.add(a1);
    assertThat(l.get(0), is(a1));
    l.add(a2);
    assertThat(l.get(1), is(a2));
  }
}
