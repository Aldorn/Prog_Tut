/**
 * Dynamic Array that will always grow according to the size needed for its content
 */
public class DynArray<T> {
    private T[] data;
    private int size;

    /**
     * Creates a new empty DynArray.
     */
    public DynArray() {
        data = new T[2];
        size = 0;
    }
    
    /**
     * Returns the element with index i.
     * Inidces are counted from 0 onwards.
     */
    public T get(int i) {
        //Simple error handling: return null for invalid index
        if (i < 0 || i >= size) return null;
        return data[i];
    }
    
    /**
     * Changes the value of the ith element.
     * The old value is replaced by the new one.
     */
    public void set(int i, T value) {
        //Simple error handling: don't change anthing for invalid index
        if (i < 0 || i >= size) return;
        data[i] = value;
    }
    
    /**
     * Returns the number of elements currently stored in the array.
     * This will always be lower or equal than the actual array's size.
     */
    public int size() {
        return size;
    }
    
    /**
     * Returns the actual size of the internal array.
     */
    public int getCapacity() {
        return data.length;
    }
    
    /**
     * Adds a value to the end of the array.
     * If the new sides woudl exceed capacity, the array will double its capacity.
     */
    public void pushBack(T value) {
        if (size == data.length) { //Capycity full
            T[] newData = new T[2 * data.length];
            for (int i = 0; i < data.length; ++i) {
                newData[i] = data[i];
            }
            data = newData;
        }
        data[size] = value;
        ++size;
    }
    
    /**
     * Returns the element at the back of the array and decreases size by one.
     * That way the last element will be deleted or overwritten later.
     * When the array only uses a quarter of its capacity after a popBack operation, it will automatically half its capacity.
     */
    public T popBack() {
        //Make sure we have elements left
        if (size == 0) return null;
        size = size - 1;
        if (size < data.length / 4) {
            T[] newData = new T[data.length / 2];
            for (int i = 0; i < data.length; ++i) {
                newData[i] = data[i];
            }
            data = newData;
        }
        return data[size];
    }    
}
