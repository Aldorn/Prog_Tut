/**
 * A simplified version of java.util.Iterator
 */
interface Iterator<E> {
    /**
     * Returns the next element in the iteration.
     */
    E next();
    
    /**
     * Returns true when the Collection has more elements.
     */
    boolean hasNext();
}
